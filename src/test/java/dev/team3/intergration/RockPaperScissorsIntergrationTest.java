package dev.team3.intergration;


import dev.team3.models.RockPaperScissor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
/*
    Rock Paper Scissors IntergrationTest
    Carlos Galvan Jr
 */
import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Rock paper scissors intergration test.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class RockPaperScissorsIntergrationTest {
    /**
     * Test rock paper scissor get returns guess.
     */
    @Test
    public void testRockPaperScissorGetReturnsGuess() {
//        String token = "test-account-auth-token";
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(
                "http://localhost:8082/rps-guess",
                String.class);
        System.out.println(response.getBody());

        assertAll(
                () -> assertNotNull(response.getBody()),
                () -> assertEquals(200, response.getStatusCodeValue())
        );
    }


}
