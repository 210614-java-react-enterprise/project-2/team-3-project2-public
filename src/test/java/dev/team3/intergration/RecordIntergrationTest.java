package dev.team3.intergration;
/*
    Record Integration Test proctored by Carlos Galvan Jr
 */
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import dev.team3.models.Record;
import dev.team3.models.RockPaperScissor;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Record intergration test.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class RecordIntergrationTest {
    /**
     * Test records get route returns an array.
     */
    @Test
    public void testRecordsGetRouteReturnsAnArray() {
        String token = "carlos-2-auth-token";
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(
                "http://localhost:8082/records?token=" + token,
                String.class);
        System.out.println(response.getBody());

        assertAll(
                () -> assertNotNull(response.getBody()),
                () -> assertEquals(200, response.getStatusCodeValue())
        );
    }

    /**
     * Test get one unauthorized returns 401.
     */
    @Test
    public void testGetOneUnauthorizedReturns401(){
        String token = "this-token-was-intentianally-created-to-be-wrong";
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response =  restTemplate.getForEntity("http://localhost:8082/records?token=" + token, String.class);
        System.out.println(response);
        assertEquals(401, response.getStatusCodeValue());
    }

    /**
     * Test records get route json is valid record object.
     */
/*
    This test converts the JSON array to POJO and verifies that it is a valid Record Object
    and also checks response code to equal 200
     */
    @Test
    public void testRecordsGetRouteJsonIsValidRecordObject() {
        String token = "carlos-2-auth-token";
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(
                "http://localhost:8082/records?token=" + token,
                String.class);
        String jsonResponse = response.getBody();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Record[] pojoFromJsonList = objectMapper.readValue(jsonResponse, Record[].class);
            for (Record r : pojoFromJsonList) {
                System.out.println(r.toStringAlternative());
            }

            assertAll(
                    () -> assertNotNull(pojoFromJsonList[0]),
                    () -> assertEquals(200, response.getStatusCodeValue())
            );

//            Record pojoObject = new Record(25,0,)
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }


//        assertNotNull(response);
    }

    /**
     * Test records post route create record.
     */
    @Test
    public void testRecordsPostRouteCreateRecord() {
        String token = "test-account-auth-token";
        RestTemplate restTemplate = new RestTemplate();
        Record testRecord = new Record();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("gameId", "0");
        map.add("amount", "123");
        map.add("token", token);
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = restTemplate.postForEntity(
                "http://localhost:8082/records/",
                request,
                String.class);

        System.out.println(response);
//        assertNotNull(response);
        //        System.out.println(response.getBody());

        assertAll(
                () -> assertNotNull(response.getStatusCode()),
                () -> assertEquals(201, response.getStatusCodeValue())
        );
    }


    /**
     * Generate request body string.
     *
     * @param id     the id
     * @param amount the amount
     * @param tkn    the tkn
     * @return the string
     */
    public String generateRequestBody(int id, int amount, String tkn) {
        return "gameId="+id+"&amount="+ amount +"&token=" + tkn;
    };

}
