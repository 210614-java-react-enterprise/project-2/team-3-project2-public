package dev.team3.integration;


import dev.team3.controllers.GameController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.web.reactive.server.WebTestClient;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.DEFINED_PORT)
public class StockGuessingGameControllerIntegrationTest {

    @Test
    void stockGuessingGameControllerEndToEndIntegrationTest(@Autowired TestRestTemplate restTemplate){
        String body = restTemplate.getForObject("/stockguessinggame",String.class);
        assertThat(body).contains("name");
    }
}
