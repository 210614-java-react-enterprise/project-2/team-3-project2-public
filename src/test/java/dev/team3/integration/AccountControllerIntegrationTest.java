package dev.team3.integration;

import dev.team3.daos.AccountRepository;
import dev.team3.models.Account;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class AccountControllerIntegrationTest {

    String url = "http://localhost:8082/";
    @Test
    public void testReturnAccountNotNull() {
        String token = "test22-auth-token";
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(
                url + "account?token=" + token,
                String.class);
        System.out.println(response.getBody());

        assertAll(
                () -> assertNotNull(response.getBody()),
                () -> assertEquals(200, response.getStatusCodeValue())
        );
    }

    @Test
    public void testReturnLeaderboardNotNull() {
        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.getForEntity(
                url + "leaderboard",
                String.class);
        System.out.println(response.getBody());

        assertAll(
                () -> assertNotNull(response.getBody()),
                () -> assertEquals(200, response.getStatusCodeValue())
        );
    }

    @Test
    public void testLoginOK() {
//        String requestBody = "username=test12&password=pass";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("username", "test22");
        map.add("password", "pass");
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.postForEntity(
                url + "login",request,
                String.class);
        assertAll(
                () -> assertEquals(200, response.getStatusCodeValue())
        );
    }

    @Test
    public void testRegisterCreated() {
        Account account = new Account();
        account.setEmail("tes42@com");
        account.setUsername("test42");
        account.setPassword("pass");
        account.setBalance(1000);


        TestRestTemplate restTemplate = new TestRestTemplate();
        ResponseEntity<String> response = restTemplate.postForEntity(
                url + "register",account,
                String.class);
        assertAll(
                () -> assertEquals(201, response.getStatusCodeValue())
        );
    }



}
