package dev.team3.services;

import dev.team3.daos.AccountRepository;
import dev.team3.daos.RecordRepository;
import dev.team3.models.Account;
import dev.team3.models.Record;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Sort;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
@Profile("dev")
public class RecordServiceTest {
    private MockMvc mockMvc;

    @MockBean
    RecordRepository recordRepository;


    @MockBean
    AccountRepository accountRepository;

    @Autowired
    private RecordService recordService;

//    @BeforeEach
//    public void setUp(){
//        this.mockMvc = MockMvcBuilders.standaloneSetup(accountRepository).build();
//    }

    @Test
    public void createNewRecordByUsernameTestNotNull(){
        Account account = new Account();
        account.setEmail("test2@com");
        account.setUsername("test2");
        account.setPassword("pass");
        account.setBalance(1000);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        doReturn(accounts).when(accountRepository).findByUsername("test2");
        account.setBalance(account.getBalance() + 100);
        Record record = new Record();
        record.setAmount(100);
        record.setGameId(1);
        record.setAccount(account);
        doReturn(record).when(recordRepository).save(record);

        assertNotNull(recordService.createNewRecordByUsername(1,100,"test2"));
    }

    @Test
    public void getRecordByUsernameLength(){
        Account account = new Account();
        account.setEmail("test5@com");
        account.setUsername("test5");
        account.setPassword("pass");
        account.setBalance(1000);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        doReturn(accounts).when(accountRepository).findByUsername("test5");
        List<Record> records = new ArrayList<>();
        Record record = new Record();
        record.setAmount(100);
        record.setGameId(1);
        record.setAccount(account);
        records.add(record);
        doReturn(records).when(recordRepository).findByAccount(account);
        assertEquals(1,recordService.getRecordByUsername("test5").get(0).getGameId());
    }



}
