package dev.team3.services;

import dev.team3.daos.StockSymbolRepository;
import dev.team3.models.StockSymbol;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import yahoofinance.Stock;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@SpringBootTest
public class StockGuessingGameServiceTest {

    private MockMvc mvc;

    @MockBean
    StockSymbolRepository stockSymbolRepository;

    @Autowired
    StockGuessingGameService stockGuessingGameService;

    @BeforeEach
    public void setUp(){
        this.mvc = MockMvcBuilders.standaloneSetup(stockSymbolRepository).build();
    }

    @Test
    public void returnServiceShouldReturnWithoutExceptionTest(){
        StockSymbol stockSymbol = new StockSymbol("AMZN");

        doReturn(stockSymbol).when(stockSymbolRepository).findOneById(any(Integer.class));

        assertNotNull(stockGuessingGameService.returnTwoGameStocks());
    }
}