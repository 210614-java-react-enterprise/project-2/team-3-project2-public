package dev.team3.services;

import dev.team3.daos.AccountRepository;
import dev.team3.models.Account;
import org.h2.tools.RunScript;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import java.util.ArrayList;
import java.util.List;

@SpringBootTest
//@Profile("dev")
public class AccountServiceTest {

    private MockMvc mockMvc;

    @MockBean
    AccountRepository accountRepository;

//    @BeforeAll
//    public static void runTeardown() throws SQLException, FileNotFoundException {
//        Connection connection = DriverManager.getConnection("jdbc:h2:~/test");
//
//        RunScript.execute(connection, new FileReader("test-setup.sql"));
//
//    }

    @Autowired
    private AccountService accountService;

    @BeforeEach
    public void setUp(){
        this.mockMvc = MockMvcBuilders.standaloneSetup(accountRepository).build();
    }


    @Test
    public void createAccountNotNull(){
        Account account = new Account();
        account.setEmail("test1@com");
        account.setUsername("test1");
        account.setPassword("pass");
        account.setBalance(1000);
        doReturn(account).when(accountRepository).save(account);
        assertNotNull(accountService.createNewAccount(account));
    }

    @Test
    public void getAccount(){
//        Account account = new Account();
//        account.setEmail("test3@com");
        Account account = new Account();
        account.setEmail("test2@com");
        account.setUsername("test2");
        account.setPassword("pass");
        account.setBalance(1000);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        doReturn(accounts).when(accountRepository).findByUsername("test2");
        assertEquals("test2@com",accountService.getAccountByUsername("test2").get(0).getEmail());
    }

    @Test
    public void getAllAccountForLeaderboardTestLength(){
        Account account = new Account();
        account.setEmail("test3@com");
        account.setUsername("test3");
        account.setPassword("pass");
        account.setBalance(1000);

        Account account2 = new Account();
        account2.setEmail("test4@com");
        account2.setUsername("test4");
        account2.setPassword("pass");
        account2.setBalance(1000);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        accounts.add(account2);
        doReturn(accounts).when(accountRepository).findAll(Sort.by(Sort.Direction.DESC, "balance"));
        assertEquals("test3@com",accountService.getAllAccountForLeaderboard().get(0).getEmail());
    }

}
