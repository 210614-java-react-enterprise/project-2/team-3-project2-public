package dev.team3.services;
/*
  proctored by Carlos Galvan Jr
 */

import dev.team3.models.RockPaperScissor;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Rock paper scissors service test.
 */
@SpringBootTest
public class RockPaperScissorsServiceTest {
    /**
     * The Rock paper scissors service.
     */
    @Autowired
    RockPaperScissorsService rockPaperScissorsService;

    /**
     * Rock paper scissors service return not null.
     */
    @Test
    public void RockPaperScissorsServiceReturnNotNull() {
        assertNotNull(rockPaperScissorsService.returnOpposingHand());
    }


    /**
     * Rock paper scissors service verify all hands.
     */
/*
    The purpose of this test is to verify that the service returns all
     3 possible hands (rock paper scissors) to make sure that its fair
     */
    @Test
    public void RockPaperScissorsServiceVerifyAllHands() {
        TreeSet<String> rpsList = new TreeSet<>();
        IntStream.range(1, 99).forEach((i) -> {
            rpsList.add(rockPaperScissorsService.returnOpposingHand().getHand());
        });
        System.out.println(rpsList);
        assertEquals(3, rpsList.size());
    }

    /**
     * Rock paper scissors service generate percentage.
     */
    @Test
    public void RockPaperScissorsServiceGeneratePercentage() {
        HashMap<String, Integer> rpsList = new HashMap<>();
        int range = 1000;
        IntStream.range(1, (range - 1)).forEach((i) -> {
            String randomHand = rockPaperScissorsService.returnOpposingHand().getHand();
            if (!rpsList.containsKey(randomHand)) {
                rpsList.put(randomHand, 1);
            }else {
                int count = rpsList.get(randomHand) + 1;
                rpsList.put(randomHand, count);
            }
        });
        System.out.println(rpsList);
        String chosenVariable = rockPaperScissorsService.returnOpposingHand().getHand();
        System.out.println((double)rpsList.get(chosenVariable)/ range );
        double average = (double) (rpsList.get("Rock") + rpsList.get("Paper") + rpsList.get("Scissors")) / rpsList.size();


        assertAll(
                ()->assertTrue(((double)rpsList.get(chosenVariable)/range) > .30 ),
                ()->assertTrue(average > .30 )
        );

    }


}
