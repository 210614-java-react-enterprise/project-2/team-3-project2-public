package dev.team3.services;


import dev.team3.daos.GameDao;
import dev.team3.models.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GameService {

    @Autowired
    private GameDao gameDao;

    public List<Game> getAllGames(){
        return gameDao.getAllGames();
    }
}
