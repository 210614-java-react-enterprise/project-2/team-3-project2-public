package dev.team3.services;

import dev.team3.daos.StockSymbolRepository;
import dev.team3.models.GameStock;
import dev.team3.models.StockSymbol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Service
public class StockGuessingGameService {

    @Autowired
    private StockSymbolRepository stockSymbolRepository;

    /**
     * Generates random integers to pull stock symbols from remote database using repository class
     * Creates GameStock objects using Yahoo Finance API to pull stock information
     * @return List containing two GameStock objects for controller layer
     */
    public List<GameStock> returnTwoGameStocks(){

        Random rand = new Random();
        int randomInt = rand.nextInt(4335);
        int randomInt2 = rand.nextInt(4335);

        while(randomInt2 == randomInt){
            randomInt2 = rand.nextInt();
        }

        StockSymbol symbol1 = stockSymbolRepository.findOneById(randomInt);
        StockSymbol symbol2 = stockSymbolRepository.findOneById(randomInt2);

        Stock stock1 = null;
        Stock stock2 = null;
        try {
            stock1 = YahooFinance.get(symbol1.getSymbol());
            stock2 = YahooFinance.get(symbol2.getSymbol());
        } catch (IOException e) {
            e.printStackTrace();
        }

        GameStock gameStock1 = new GameStock(stock1.getName(),stock1.getSymbol(),stock1.getQuote().getPrice());
        GameStock gameStock2 = new GameStock(stock2.getName(),stock2.getSymbol(),stock2.getQuote().getPrice());

        return Arrays.asList(gameStock1,gameStock2);
    }
}
