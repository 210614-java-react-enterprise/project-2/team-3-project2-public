package dev.team3.services;

import dev.team3.daos.AccountRepository;
import dev.team3.models.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class AccountService {


    @Autowired
    private AccountRepository accountRepo;

    /**
     * return all account objects in a list for leaderboard
     * @return a list of account for leaderboard
     */
    public List<Account> getAllAccountForLeaderboard(){
        return accountRepo.findAll(Sort.by(Sort.Direction.DESC, "balance"));
    }

//    public Account getAccountById(int id){
//        return accountRepo.getOne(id);
//    }

    /**
     * return the target account object given the username
     * @param username
     * @return the target account object
     */
    public List<Account> getAccountByUsername(String username){
        return accountRepo.findByUsername(username);
    }

    /**
     * create a new account using given account object
     * @param a an Account
     * @return the new account
     */
    public Account createNewAccount(Account a){
        return accountRepo.save(a);
    }

}

