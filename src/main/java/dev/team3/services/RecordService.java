package dev.team3.services;

import dev.team3.daos.AccountRepository;
import dev.team3.daos.RecordRepository;
import dev.team3.models.Account;
import dev.team3.models.Record;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RecordService {


    @Autowired
    private RecordRepository recordRepo;
    @Autowired
    private AccountRepository accountRepo;

    /**
     * return all records for given username
     * @param username
     * @return all records for given username
     */
    public List<Record> getRecordByUsername(String username){
        Account account = accountRepo.findByUsername(username).get(0);

        return recordRepo.findByAccount(account);
    }


//    public Record createNewARecord(Record r){
//        return recordRepo.save(r);
//    }

    /**
     * create a new record given (gameId, amount, username)
     * @param gameId
     * @param amount
     * @param username
     * @return a new record
     */
    public Record createNewRecordByUsername(int gameId, double amount, String username){
        Account account = accountRepo.findByUsername(username).get(0);
        account.setBalance(account.getBalance() + amount);
        accountRepo.save(account);
        Record record = new Record();
        record.setAmount(amount);
        record.setGameId(gameId);
        record.setAccount(account);
        System.out.println(record.toString());;
        return recordRepo.save(record);
    }


}
