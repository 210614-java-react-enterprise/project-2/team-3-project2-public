package dev.team3.services;

import dev.team3.daos.RockPaperScissorsDao;
import dev.team3.models.RockPaperScissor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * The type Rock paper scissors service.
 * Created by Carlos Galvan Jr
 */
@Service
public class RockPaperScissorsService {

    @Autowired
    private RockPaperScissorsDao rockPaperScissorsDao;

    /**
     * Return opposing hand rock paper scissor.
     *
     * @return the rock paper scissor
     */
    public RockPaperScissor returnOpposingHand() {
        return rockPaperScissorsDao.returnOpposingHand();
    }

    /**
     * Return victory message string.
     *
     * @param bot  the bot
     * @param user the user
     * @return the string
     */
    public String returnVictoryMessage(RockPaperScissor bot, String user){
        return rockPaperScissorsDao.victoryChecker( bot,  user);
    }
}
