package dev.team3.daos;

import dev.team3.models.RockPaperScissor;

/**
 * The interface Rock paper scissors dao.
 * Created by Carlos Galvan Jr
 */
public interface RockPaperScissorsDao {
    /**
     * Return opposing hand rock paper scissor.
     *
     * @return the rock paper scissor
     */
    RockPaperScissor returnOpposingHand();

    /**
     * Victory checker string.
     *
     * @param bot  the bot
     * @param user the user
     * @return the string
     */
    String victoryChecker(RockPaperScissor bot, String user);
}
