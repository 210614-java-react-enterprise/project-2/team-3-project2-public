package dev.team3.daos;

import dev.team3.models.Game;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
public class GameDaoImpl implements GameDao{

    private List<Game> games = new ArrayList<Game>();

    public GameDaoImpl(){
        games.add(new Game("Rock Paper Scissors"));
        games.add(new Game("Stock Guessing Game"));
    }

    @Override
    public List<Game> getAllGames() {
        return new ArrayList<>(games);
    }

}
