package dev.team3.daos;

import dev.team3.models.StockSymbol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StockSymbolRepository extends JpaRepository<StockSymbol, Integer> {
    /**
     * @param id
     * @return StockSymbol object for StockGuessingGameService to use
     */
    StockSymbol findOneById(int id);
}
