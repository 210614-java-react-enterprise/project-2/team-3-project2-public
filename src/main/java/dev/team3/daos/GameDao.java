package dev.team3.daos;

import dev.team3.models.Game;

import java.util.List;

public interface GameDao {

    List<Game> getAllGames();
}
