package dev.team3.daos;

import dev.team3.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository extends JpaRepository<Account, Integer> {

    List<Account> findByUsername(String userName);
    List<Account> findByEmail(String email);

}

