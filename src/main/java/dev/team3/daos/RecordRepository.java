package dev.team3.daos;

import dev.team3.models.Account;
import dev.team3.models.Record;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RecordRepository extends JpaRepository<Record, Integer> {
    List<Record> findByAccount(Account account);

}
