package dev.team3.daos;

import dev.team3.models.RockPaperScissor;
import org.springframework.stereotype.Repository;

import java.util.Random;

/**
 * The type Rock paper scissors dao.
 * Created by Carlos Galvan Jr
 */
@Repository
public class RockPaperScissorsDaoImpl implements RockPaperScissorsDao {
    @Override
    public RockPaperScissor returnOpposingHand() {
        Random rand = new Random();

        return new RockPaperScissor(rand.nextInt(3));
    }

    @Override
    public String victoryChecker(RockPaperScissor bot, String user) {

        System.out.println(bot);
        if (bot.getWeakness().equals(user)) {
            return "Congrats! You won this round!";
        } else if (bot.getEffective().equals(user)) {

            return "Sorry, You lost this round";
        } else if (bot.getHand().equals(user)) {
            return "Draw";
        }

        return null;
    }
}
