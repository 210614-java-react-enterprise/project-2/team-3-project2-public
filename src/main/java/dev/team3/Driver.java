package dev.team3;

import dev.team3.models.Account;
import dev.team3.services.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.AliasFor;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * The type Driver.
 */
@SpringBootApplication
@EnableSwagger2
public class Driver {

    @Autowired
    private AccountService accountService;

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

        SpringApplication.run(Driver.class, args);
    }

    /**
     * Api docket.
     *
     * @return the docket
     */
    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("dev.team3.controllers"))
                .paths(PathSelectors.any())
                .build();
    }


}
