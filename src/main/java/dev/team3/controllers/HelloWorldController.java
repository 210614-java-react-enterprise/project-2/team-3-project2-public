package dev.team3.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@CrossOrigin
public class HelloWorldController {

    @GetMapping("/hello")
    @ResponseBody
    public String displayMessage(){
        return "Hello World from my spring boot app!";
    }



}
