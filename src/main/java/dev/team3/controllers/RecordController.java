package dev.team3.controllers;

import dev.team3.models.Account;
import dev.team3.models.Record;
import dev.team3.services.AccountService;
import dev.team3.services.RecordService;
import dev.team3.util.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/records")
@CrossOrigin
public class RecordController {

    @Autowired
    private RecordService recordService;

    /**
     * return the record history for given token
     * @param token
     * @return ResponseEntity with list of records(json) and OK status code if username is found.
     *      * Otherwise, return ResponseEntity with UNAUTHORIZED status code
     */
    @GetMapping(produces = "application/json")
    //    @ResponseBody
        public ResponseEntity<List<Record>> returnHistory(@RequestParam("token") String token){
        //updated to handle unauthorized tokens
        try{
            String username = token.substring(0,token.length()-11);
            System.out.println(recordService.getRecordByUsername(username));
            return new ResponseEntity<>(recordService.getRecordByUsername(username), HttpStatus.OK);
        }catch (IndexOutOfBoundsException e){
            List<Record> unauthorized = new ArrayList<>();
            return new ResponseEntity<List<Record>>(unauthorized, HttpStatus.UNAUTHORIZED);
        }


    }

    /**
     * create a new record given (gameId, amount, username)
     * @param gameId
     * @param amount
     * @param token
     * @return ResponseEntity with the created record object (json) and 201 status code.
     */
    @PostMapping(consumes = "application/x-www-form-urlencoded", produces = "application/json")
//    @ResponseBody
    public ResponseEntity<Record> createNewRecord(@RequestParam("gameId") int gameId,
                                                  @RequestParam("amount") double amount,
                                                  @RequestParam("token") String token){
        String username = token.substring(0,token.length()-11);
        System.out.println("Creating record for user " + username);

        return new ResponseEntity<>(recordService.createNewRecordByUsername(gameId, amount, username), HttpStatus.CREATED);
    }

}
