package dev.team3.controllers;

import dev.team3.models.Game;

import dev.team3.models.GameStock;
import dev.team3.services.GameService;
import dev.team3.services.StockGuessingGameService;

import dev.team3.models.RockPaperScissor;
import dev.team3.services.GameService;
import dev.team3.services.RockPaperScissorsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * The type Game controller.
 */
@RestController
@CrossOrigin
public class GameController {

   @Autowired
   private GameService gameService;

   @Autowired
   private StockGuessingGameService stockGameService;


   /**
    * Http request mapped by /games path
    * @return List of Games as Json for frontend
    */
   @GetMapping(value = "/games", produces = "application/json")
   public List<Game> returnAllGames() {
      return gameService.getAllGames();
   }

   /**
    * http request mapped by /stockguessinggame path
    * @return two GameStock objects as Json for frontend
    */
   @GetMapping(value="/stockguessinggame", produces="application/json")
   public List<GameStock> returnGameStocks(){
      return stockGameService.returnTwoGameStocks();
   }

   // **** javascript make get request to /stockguessing

   // return json string with 2 stocks
   //   // javascript parses json that it receives from get
   //   // use that json to create 2 options for the client
   //   // the client picks between their options and set a bet amt
   //   // javascript is gonna make a modal "you won" or "you lost"
   //
   //   // **** javascript is going to make a post request to /stockguessing header post request body whatever changes to user balance / earnings (for leaderboard)
   //
   //   // balance changes positively or negatively

   @Autowired
   private RockPaperScissorsService rockPaperScissorsService;

   /**
    * Return opposing hand rock paper scissor.
    *
    * @return the rock paper scissor
    */
   @GetMapping(value = "/rockpaperscissor", produces = "application/json")
   public RockPaperScissor returnOpposingHand() {

      return rockPaperScissorsService.returnOpposingHand();
   }

   /**
    * Return opposing hand json response entity.
    *
    * @return the response entity
    */
   @GetMapping(value = "/rps-guess", produces = "application/json")
   public ResponseEntity<RockPaperScissor> returnOpposingHandJson() {
      return new ResponseEntity<RockPaperScissor>(rockPaperScissorsService.returnOpposingHand(), HttpStatus.OK);
   }


}
