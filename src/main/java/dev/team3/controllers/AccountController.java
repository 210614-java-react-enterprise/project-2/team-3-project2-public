package dev.team3.controllers;

import dev.team3.models.Account;
import dev.team3.models.Record;
import dev.team3.services.AccountService;
import dev.team3.util.EmailUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@Controller
@RestController
@CrossOrigin
//@RequestMapping("/accounts")
public class AccountController {


    @Autowired
    private AccountService accountService;

//    @GetMapping(produces = "application/json")
////    @ResponseBody
//    public ResponseEntity<List<Account>> returnAllAccounts(@RequestParam(value = "id",required = false)String username){
//        if(username != null){
//            return new ResponseEntity<>(accountService.getAccountByUsername(username), HttpStatus.OK);
//        }else{
//            return new ResponseEntity<>(accountService.getAllAccount(), HttpStatus.OK);
//        }
//    }

    /**
     *  return the target account given token(session storage)
     * @param token
     * @return ResponseEntity with the target account(json) and Ok status code
     */
    @GetMapping(value="/account", produces = "application/json")
    //    @ResponseBody
    public ResponseEntity<Account> returnAccount(@RequestParam("token") String token){
        String username = token.substring(0,token.length()-11);
        Account account = accountService.getAccountByUsername(username).get(0);
        return new ResponseEntity<>(account, HttpStatus.OK);

    }

    /**
     * return the leaderboard
     * @return ResponseEntity with the list of account objects(json) for leaderboard and Ok status code
     */
    @GetMapping(value="/leaderboard",  produces = "application/json")
//    @ResponseBody
    public ResponseEntity<List<Account>> returnLeaderboard(){
        return new ResponseEntity<>(accountService.getAllAccountForLeaderboard(),HttpStatus.OK);
    }
//    @GetMapping("/{id}")
////    @ResponseBody
//    public ResponseEntity<Account> returnTrackById(@PathVariable("id")int id){
//        return new ResponseEntity<>(accountService.getAccountById(id), HttpStatus.OK);
//    }

    /**
     * register account given account object
     * @param account
     * @return ResponseEntity with the created account objects(json)  and 201 status code
     */
    @PostMapping(value="/register", consumes = "application/json", produces = "application/json")
//    @ResponseBody
    public ResponseEntity<Account> createNewAccount(@RequestBody Account account){
          account.setBalance(1000);
          String subject = "Thank you for creating an account";
          String text = "Your username is " + account.getUsername();
          EmailUtil.sendEmail(account.getEmail(), subject, text);
        return new ResponseEntity<>(accountService.createNewAccount(account), HttpStatus.CREATED);
    }

    /**
     * attempt login and check if username and password are correct
     * @param username
     * @param password
     * @return ResponseEntity with token  and OK status code if username and password are correct.
     * Otherwise, return ResponseEntity with UNAUTHORIZED status code
     *
     */
    @PostMapping(value="/login", consumes = "application/x-www-form-urlencoded")
//    @ResponseBody
    public ResponseEntity<String> login(@RequestParam("username") String username,
                                         @RequestParam("password") String password){
        System.out.println("log in ...");
        List<Account> accounts = accountService.getAccountByUsername(username);
        if(accounts.size() > 0){
            Account account = accounts.get(0);
            System.out.println(account);

            System.out.println(account.getPassword().equals(password));
            if(account.getPassword().equals(password)){
                System.out.println("log in:ok");
                return ResponseEntity.ok().header("Authorization", username+"-auth-token")
                        .header("Access-Control-Expose-Headers", "Content-Type, Allow, Authorization").build();
            }
        }
        System.out.println("login failed");

        return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }


}

