package dev.team3.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

public class GameStock {

    private String name;

    private String symbol;

    private BigDecimal price;

    public GameStock(){
        super();
    }

    public GameStock(String symbol){
        this.symbol = symbol;
    }

    public GameStock(String name, String symbol, BigDecimal price){
        this.name = name;
        this.symbol = symbol;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameStock gameStock = (GameStock) o;
        return Objects.equals(name, gameStock.name) && Objects.equals(symbol, gameStock.symbol) && Objects.equals(price, gameStock.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, symbol, price);
    }

    @Override
    public String toString() {
        return "GameStock{" +
                "name='" + name + '\'' +
                ", symbol='" + symbol + '\'' +
                ", price=" + price +
                '}';
    }
}
