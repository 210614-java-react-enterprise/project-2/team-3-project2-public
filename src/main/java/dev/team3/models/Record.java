package dev.team3.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.Objects;

@Component
@Entity
@Table(name = "record")
@JsonIgnoreProperties({"handler", "hibernateLazyInitializer"})
public class Record {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "record_id")
    private int id;

    @Column(name = "game_id")
    private int gameId;

    private double amount;


    @SuppressWarnings("unchecked")
    @JsonProperty("account")
    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    public Record(){

    }
    //record constructor created by carlos for testing
    public Record(int id, int gameId, double amount, Account account) {
        this.id = id;
        this.gameId = gameId;
        this.amount = amount;
        this.account = account;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGameId() {
        return gameId;
    }

    public void setGameId(int gameId) {
        this.gameId = gameId;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return id == record.id && gameId == record.gameId && Double.compare(record.amount, amount) == 0 && Objects.equals(account, record.account);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, gameId, amount, account);
    }

    public String toStringAlternative() {
        return "Record{" +
                "id=" + id +
                ", gameId=" + gameId +
                ", amount=" + amount +
                ", account=" + account +
                '}';
    }
}




