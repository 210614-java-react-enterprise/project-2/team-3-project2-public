package dev.team3.models;

import java.util.Arrays;
import java.util.Objects;

/**
 * The type Rock paper scissor.
 * Created by Carlos Galvan Jr
 */
public class RockPaperScissor {
    private final String[] hands = {"Rock","Paper","Scissors"};

    private String hand;
    private String weakness;
    private String effective;

    /**
     * Instantiates a new Rock paper scissor.
     */
    public RockPaperScissor() {
    }

    /**
     * Instantiates a new Rock paper scissor.
     *
     * @param hand the hand
     */
    public RockPaperScissor(int hand) {
        this.hand = hands[hand];
        setWeakness(hand);
        setEffective(hand);
    }

    /**
     * Instantiates a new Rock paper scissor.
     *
     * @param hand the hand
     */
    public RockPaperScissor(String hand) {
        this.hand = hand;
        
    }

    /**
     * Gets hand.
     *
     * @return the hand
     */
    public String getHand() {
        return hand;
    }

    /**
     * Sets hand.
     *
     * @param hand the hand
     */
    public void setHand(int hand) {
        this.hand = hands[hand];
    }

    /**
     * Gets weakness.
     *
     * @return the weakness
     */
    public String getWeakness() {
        return weakness;
    }

    /**
     * Gets effective.
     *
     * @return the effective
     */
    public String getEffective() {
        return effective;
    }

    /**
     * Sets weakness.
     *
     * @param hand the hand
     */
    public void setWeakness(int hand) {

        this.weakness = hands[(hand + 1) % hands.length];
    }

    /**
     * Sets effective.
     *
     * @param hand the hand
     */
    public void setEffective(int hand) {
        this.effective = hands[(hand - 1 + hands.length) % hands.length];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RockPaperScissor that = (RockPaperScissor) o;
        return Arrays.equals(hands, that.hands) && Objects.equals(hand, that.hand) && Objects.equals(weakness, that.weakness) && Objects.equals(effective, that.effective);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(hand, weakness, effective);
        result = 31 * result + Arrays.hashCode(hands);
        return result;
    }

    @Override
    public String toString() {
        return "RockPaperScissor{" +
                ", hand='" + hand + '\'' +
                ", weakness='" + weakness + '\'' +
                ", effective='" + effective + '\'' +
                '}';
    }
}
